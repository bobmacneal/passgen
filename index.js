#!/usr/bin/env node
const createPassword = require('./utils/createPassword')
const savePassword = require('./utils/savePassword')
const clipboard = require('clipboardy')

const program = require('commander')
program.version('1.0.0').description('Command Line Password Generator')
program
  .option('-l, --length <number>', 'length of password', '8')
  .option('-s, --save', 'save password to passwords.txt')
  .option('-nn, --no-numbers', 'remove numbers')
  .option('-ns, --no-symbols', 'remove symbols')
  .parse()

const { length, numbers, save, symbols } = program.opts()
const generatedPassword = createPassword(length, numbers, symbols)
if (save) {
  savePassword(generatedPassword)
}
clipboard.writeSync(generatedPassword)

console.log(`Password: ${generatedPassword}`)
console.log('Password copied to clipboard')
